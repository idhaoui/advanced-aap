# README

This repository contains the [Hugo](https://gohugo.io/) markdown sources to create [https://ansible-labs-crew.gitlab.io/](https://ansible-labs-crew.gitlab.io/). The web site is automatically updated when making changes to the `main` branch. Please use issues and merge requests to propose or apply changes.

Use the [issue tracker](https://gitlab.com/ansible-labs-crew/ansible-labs-crew.gitlab.io/-/issues) to file bugs or propose improvements. Please use the `bug` or `feature request` label to make filtering easy.

## Workflow

1. maintain latest version of workshops in the `/content/<workshop>` directory

1. after finishing a major event, for example Red Hat Summit or AnsibleFest, archive the current version in the `/content/archive/<year>` folder
