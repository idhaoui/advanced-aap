+++
title = "Project Signing"
weight = 11
+++

## OPTIONAL EXERCISE

Project signing is a new feature in Red Hat Ansible Automation Platform 2.3. Users can sign content like playbooks, inventories, etc that lives in a project (backed by a version control repository) and configure Automation Controller to verify whether or not that content has been changed form the signed versions. This way users can make sure the supply chain remains untampered, from the source repository to running an automation job.

The best way to understand how this works is, as most times, to give it a try. Read on!

## Setting up a Git repository

The first thing we need is a repository for the content we are going to sign, we'll use a Git repo. We'll just create a simplistic Git server on our control host (like we did in the structured content chapter). In real life you would use GitLab, Gitea, or any other Git server. We have prepared a Playbook to set it up, just run:

```bash
wget https://gitlab.com/ansible-labs-crew/ansible-labs-crew.gitlab.io/-/raw/main/content/ansible-controller-advanced/11-repo-sign/repo-sign-git.yml
```

Modify the `repo-sign.yml` and replace the \<LABID> in the `hosts:` attribute at the top of the playbook and in the `generate public key` task. After applying that change, run the playbook with `ansible-navigator`.

```bash
ansible-navigator run repo-sign-git.yml -i bastion.<LABID>.internal,
```

{{% notice info %}}
The `-i` parameter forces Ansible to interact with a host, which is not listed in our inventory file in `/etc/ansible/hosts`. The trailing ',' is part of the command!
{{% /notice %}}

Next we will clone the repository from our "Git server" (actually a Git repo in `/home/git/` reachable via SSH) to your home directory. To enable you to work with git on the command line the SSH key for your current user was already added to the Git user **git**. Next, clone the repository on the control machine, and configure git with your details (replace \<LABID>):

```bash
git clone git@bastion.<LABID>.internal:/home/git/projects/repo-sign.git
# Message "warning: You appear to have cloned an empty repository." is OK and can be ignored
git config --global push.default simple
git config --global user.name "Your Name"
git config --global user.email you@example.com
cd repo-sign/
```

{{% notice tip %}}
The repository is currently empty. The three config commands are just there to avoid warnings from Git.
{{% /notice %}}

You are now going to add some content, namely just a simple Playbook (this is for learning purposes, so we won't use roles etc):

Still as user `lab-user` create the file `/home/lab-user/repo-sign/install-apache.yml` with the following content:

```yaml
---
- name: Apache server installed
  hosts: all

  tasks:
  - name: latest Apache version installed
    yum:
      name: httpd
      state: latest

  - name: latest firewalld version installed
    yum:
      name: firewalld
      state: latest

  - name: firewalld enabled and running
    service:
      name: firewalld
      enabled: true
      state: started

  - name: firewalld permits http service
    firewalld:
      service: http
      permanent: true
      state: enabled
      immediate: yes

  - name: Apache enabled and running
    service:
      name: httpd
      enabled: true
      state: started
```

Now we need to add the content to the repository and push it:

```bash
git add install-apache.yml
git commit -m "Adding playbook"
git push
```

## Sign Repository Content

By now we have a Git repository with a simple Playbook, nothing special yet. Now we'll sign the repository content.

We will create a GnuPG key pair to sign our project locally. Asynchronous keys are used to validate the integrity of a project’s content. The private key from the key pair is used at the point of signing and a public key is used to verify that the content has not been manipulated in any way. The public key will be used in automation controller to verify signatures later on.

### Prepare GPG

First we have to create the keys we'll use with GPG. Create the file `/home/lab-user/gpg.txt` with the following content:

```raw
%echo Generating a basic OpenPGP key
Key-Type: default
Key-Length: 4096
Subkey-Type: default
Subkey-Length: default
Name-Real: Instruqt
Name-Comment: with no passphrase
Name-Email: student@localhost
Expire-Date: 0
%no-ask-passphrase
%no-protection
# Do a commit here, so that we can later print "done" :-)
%commit
%echo done
```

Think of this file as the instructions given to the GnuPG library on the system about the type of key pair we want to create. This creates a key pair without a passphrase.

Now create a GPG key pair using the gpg.txt file. Run the following commands in the Terminal window to create the key pair.

The first command will create the key pair using the gpg.txt file:

```bash
gpg --batch --gen-key ~/gpg.txt
```

The next command will export the public key between the plaintext headers and footers in the file called signing_demo.asc. This is a more conventional method of sharing public keys.

```bash
gpg --output ~/signing_demo.asc --armor --export
```

Check the content of the exported public key using the below command.

```bash
cat ~/signing_demo.asc
```

We will use all the contents of this file in the next steps when we show the verification of projects in automation controller.

And finally run the below command to list the gpg keys that exist on the system, this should have only one key pair, the one just created.

```bash
gpg --list-keys
```

### Configure ansible-sign and sign playbooks

Now that we have prepared the signing keys let's sign our `repo-sign` repository using the `ansible-sign` tool. First install the tool itself:

```bash
sudo yum install ansible-sign -y
```

Create a MANIFEST.in file in the local repo by running the following command in the terminal window:

```bash
cat << EOF > /home/lab-user/repo-sign/MANIFEST.in
recursive-exclude .git *
include install-apache.yml
EOF
```

The above command will create a new file called `MANIFEST.in` in the `repo-sign` local repo. This file acts as the directive to the `ansible-sign` tool as to which files it will sign. In the next task we will show how to sign a local project using the ansible-sign CLI tool.

Use `ansible-sign` to create signatures. Run the following commands in the Terminal window to create the signatures using the `ansible-sign` CLI:

```bash
ansible-sign project gpg-sign /home/lab-user/repo-sign
```

The above command tells the `ansible-sign` tool to gpg-sign the `repo-sign` project locally.

Once you run the command, you will see that the GPG signing was successful and you can confirm that by looking at the new files that were created in the repo-sign repo locally. `ansible-sign` picked up the `MANIFEST.in` file from the repo-sign directory and used that to sign the mentioned files in it, if you look closely it says `recursive-exclude` to all the files in the `.git/` directory. This tells the `ansible-sign` tool to not sign any files in the `.git` folder.

Have a look at the files `ansible-sign` created:

```bash
cat .ansible-sign/sha256sum.txt
cat .ansible-sign/sha256sum.txt.sig
```

The above command shows the new files created by `ansible-sign` in the project repository to store the checksums and signatures of the files it signed. These files help the automation controller in validation of the signatures.

### Push content to Git Repo

Now push the new files from our local Git clone to the Git repo. Add the new files for git staging:

```bash
git add .ansible-sign/ MANIFEST.in install-apache.yml
```

Commit the changes:

```bash
git commit -m "Adding signatures for project"
```

Push the new files to our Git repo server by SSH:

```bash
git push
```

## Setup Automation Controller

Now we have to setup Automation Controller to verify our signed content.

### Create Credential with signing public key

The first step is to create a new Credential with the public key file that we exported already. In the Terminal, run:

```bash
cat ~/signing_demo.asc
```

Copy all of its contents with headers and footers. Go to the Automation Controller web UI tab and go **Ressources > Credentials** on the left-hand side menu:

* click on the blue **Add** button.
* **Name**: ansible-sign
* **Credential Type**: GPG Public Key
* This will open a textbox for you to paste the public key that is in the Clipboard. Paste the public key and click **Save**.

This will add the public key as a Credential in your Automation Controller.

### Create Project with signed content

To access the Git repository hosted on the bastion node via SSH we need an SCM credential. First in the Terminal print the SSH key:

**If you did the "Well Structured Content Repositories" chapter this Credential already exists and can be used. Skip the next section and jump to creating the Project.**

```bash
cat ~/.ssh/<LABID>.pem
```

Copy the complete key.

In the Automation Controller web UI click on **Resources > Credentials** on the left-hand side menu:

* Click on the blue **Add** button.
* **Name**: git
* **Credential Type**: Source Control
* **Username**: git
* This will open a textbox for you to paste the public key that is in the Clipboard. Paste the public key into the **SCM Private Key** field
* Click **Save**.

This will add the public key as a Credential in your Automation Controller.

In the Automation Controller web UI click on **Resources > Projects** on the left-hand side menu:

* Click on the blue **Add** button
* **Name**: Signed Project
* **Source Control Type**: Git
* **Content Signature Validation Credential**: ansible-sign
* **Source Control URL**: git@bastion.\<LABID>.internal:/home/git/projects/repo-sign
* Replace \<LABID>!
* Click **Save**

This way we have added the public key as Credential and created a project in Automation Controller to use the public key credential for the project that we added signatures for. Now we can check in Automation Controller if the added signatures were validated.

## Check if signatures were validated in controller

Let's check if the signatures have been checked during the initial Project sync.

Click on **Jobs** on the left menu and then click the most recent job that ran the project sync for the **Signed Project**. If this job was successful, that means the signature validation was successful. You can also verify this by looking at the tasks in the job run, scroll the job run to find the below tasks:

```bash
PLAY [Perform project signature/checksum verification] *************************

TASK [Verify project content using GPG signature] ******************************
ok: [localhost]

TASK [Verify project content against checksum manifest] ************************
ok: [localhost]
```

Above tasks indicate that the signature validation was successful.

## Check with tampered Content

The last step in this lab is to check if Automation Controller picks up if the content has been changed without re-signing it, indicating somebody has tampered with the content.

Open the file `/home/lab-user/repo-sign/install-apache.yml` and change something, e.g. set firewalld to `state: disabled`. Save the changes.

Push the new version to the Git repo:

```bash
git add install-apache.yml
git commit -m "changing file"
git push
```

If this change was legit, you would have re-signed the repository content. This way Automation Controller should recognize the signature of the file is not valid anymore and fail the repo sync. Give it a try:

* Go to the Automation Controller web UI **Ressources > Project** and start a sync of the **Signed Project** repository.
* Check the result in the **jobs** list!

The sync job should fail and the output should clearly indicate why:

```bash
TASK [Verify project content using GPG signature] ******************************
ok: [localhost]

TASK [Verify project content against checksum manifest] ************************

fatal: [localhost]: FAILED! => {"changed": false, "msg": "Checksum mismatch: install-apache.yml"}
```

We changed a signed file and skipped signing it to see the behavior in Automation Controller. Automation controller failed the verification and shows which files were not signed in the Project.
