+++
title = "Automation Mesh"
weight = 7
+++

Ansible Automation Platform (AAP) 2.1 introduced a new key component that changes the way Ansible Automation Platform operates: **Automation mesh**.

Running automation at a large scale is challenging, you have to work across different platforms, network links and locations. You want to still manage your automation centrally but rest assured it's executed reliably from the data center to edge sites with limited connectivity.

The solution is to bring and execute your automation closer to the devices and technologies you manage. Here automation mesh comes into play. It gives maximum flexibility in planning distributed, remote or otherwise complex automation deployments.

Automation mesh works by separating the **control plane** (managing your automation) and the **execution plane** (running your Ansible content). It does this by introducing different AAP node types:

* **Control nodes** for pure management capabilities (i.e. Automation Controller's core)
* **Hybrid nodes** can both control and execute automation, think single-node Automation Controller.
* **Execution nodes** for pure execution capabilities, run jobs under ansible-runner with podman isolation.
* **Hop nodes** are similar to a jump host, hop nodes will route traffic to other execution nodes. Hop nodes cannot execute automation nor management tasks.

But this is not a blog post, we want to give you a hands-on introduction to this exciting new part of Ansible Automation Platform.

## Exploring the Automation mesh

### The Topology

First go and check the automation mesh setup of your Automation Controller cluster under **Administration** and then **Topology View**. You will see a three node mesh representation consisting of hybrid nodes (labelled **Hy**) which are your controllers and a forth node labelled **Ex**, this is your execution node.

![mesh topology](../../images/mesh-topology.png)

If you move your mouse over a node it shows the relationships, if you click it you get some more details including the health status.

### Adding Execution Nodes

Adding new mesh nodes is easy using the installer, but the needed installer run would take too long for this lab. You basically have to include the node(s) into the installer inventory by adding some lines like this:

```ini
[execution_nodes]
<hostname1>
<hostname2>

[automationcontroller:vars]
peers=execution_nodes
```
And then run the AAP installer.
### Diving Deeper

To get a feeling how automation mesh works, from a VS Code terminal ssh to your automation controller 1 and become `root`:

```bash
ssh autoctl1.<LABID>.internal
sudo -i
```

**Every automation mesh node** runs a daemon `receptor`, have a look at the service:

```bash
$ systemctl status receptor.service
● receptor.service - Receptor
   Loaded: loaded (/usr/lib/systemd/system/receptor.service; enabled; vendor preset: disabled)
  Drop-In: /etc/systemd/system/receptor.service.d
           └─override.conf
   Active: active (running) since Mon 2022-08-22 08:07:23 UTC; 1h 1min ago
 Main PID: 46520 (receptor)
    Tasks: 6 (limit: 23416)
   Memory: 22.0M
   CGroup: /system.slice/receptor.service
           └─46520 /usr/bin/receptor -c /etc/receptor/receptor.conf
```

The configuration file `/etc/receptor/receptor.conf` holds all the gritty details, if e.g. we want to know the (configurable) port mesh is using, run:

```bash
$ grep port /etc/receptor/receptor.conf
    port: 27199
```

Here the default port `27199` is used. Automation mesh comes with a handy utility called `receptorctl`, to get an overview of your mesh setup still on controller 1 run:

```bash
receptorctl --socket /var/run/awx-receptor/receptor.sock status
```

### Use the Execution Node

Right now you have one execution node that might be located in a remote data center. Now let's make sure automation jobs for managed nodes that are in the same remote data center as the execution node are run **on** the execution node.

{{% notice tip %}}
Of course we are pretending here, in our lab setup all nodes run side by side. Does this matter? No... ;)
{{% /notice %}}

The first step is do create a new **Instance Group** containing the execution node:

* In the automation controller web UI open **Instance Groups**
* Add a new **Instance Group** named `remote-dc`
* Click on **Instances** for your new instance group
* **Associate** your execution node `exec1.<LABID>.internal` with the group
* **Enable** the node (you disabled it in a previous task) using the slider to the right ofthe node.

Next we will disassociate the execution node from the `default` instance group:

* In the automation controller web UI open **Instance Groups**
* Click on the `default` **Instance Group**
* Click on **Instances**
* Select the node with the name `exec1.<LABID>.internal`
* Click on **Disassociate** to disassociate the execution node from the `default` **Instance Group** to make sure it's dedicated to the `remote-dc` group.
* Click on **Disassociate** in the dialog box to confirm.

Now we need an inventory that represents the managed nodes in the remote data center:

* In automation controller web UI open **Resources** and **Inventories**
* create a new **Inventory** `Remote DC`
* For **Instance Groups** click on the magnifying glass and select the `remote-dc` Instance Group and click **Select**
* Click **Save**

Now we have an inventory that is tied to an execution node via an Instance Group. The only thing left is to put one of the managed nodes into the inventory:

* Click on the **Hosts** tab in your `Remote DC` inventory
* Click on **Add** to add a host to the inventory
* Add your host `node3.<LABID>.internal` to the `Remote DC` Inventory.
* Navigate to the `Lab Inventory`
* Click on the **Hosts** menu
* Select the check box next to `node3.<LABID>.internal` and click **Delete** to remove the host from the `Lab Inventory`
* Confirm to delete the host from the `Lab Inventory`

Now we're all set, let's test it!

* Still in the web UI, go to **Templates** and copy the `Apache Install` template using the copy-icon to the right.
* Edit the copied **Template** and change the **Inventory** to `Remote DC`
* **Save** and **Launch** the new template

After the template run has finished, have a look at the **Details** tab of the job: For execution node it should show your `exec1.<LABID>.internal` node.

{{% notice tip %}}
Of course this is the most simple example of an automation mesh. There is much more to it; you can build a large distributed automation platform with redundant data paths and execution nodes. Give it a go!
{{% /notice %}}

### Tips

[Here](https://www.ansible.com/blog/topic/automation-mesh) are some great blog posts covering automation mesh.
